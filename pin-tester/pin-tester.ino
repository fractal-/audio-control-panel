//By Bozarre

int outputs[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
int i = 0;
int maxOutputs = 20;

void setup() {
  // initialize digital pin as outputs.
  Serial.begin(9600);
  Serial.println("Init");
  for (i = 0; i < maxOutputs; i++)
  {
    Serial.print("init pin ");
    Serial.println(outputs[i]);
    pinMode(outputs[i], OUTPUT);
  }
}

// the loop function runs over and over again forever
void loop() {
  for (i = 0; i < maxOutputs; i++)
  {
    Serial.print("test pin ");
    Serial.println(outputs[i]);
    digitalWrite(outputs[i], HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(500);                       // wait for a second
    digitalWrite(outputs[i], LOW);    // turn the LED off by making the voltage LOW
    delay(250); // wait for a second
  }
}
