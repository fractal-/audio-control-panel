#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=icon_audio_control.ico
#AutoIt3Wrapper_Outfile=Audio Controller.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <TrayConstants.au3>
#Include <String.au3>
#include 'CommMG.au3'
#include 'volume_changer.au3'

;COM Vars
Global $DefaultCmPort = 3       ; Port
Global $CmBoBaud = 9600         ; Baud
Global $CmboDataBits =  8       ; Data Bits
Global $CmBoParity = "none"     ; Parity
Global $CmBoStop = 1            ; Stop
Global $setflow = 2             ; Flow

;Internal for the Serial UDF
Global $sportSetError = ''

HotKeySet("{F5}","stop")


Global $activated_apps_switches[4]
Global $volume_change_selected_apps = 0.05
Global $volume_change_focused_app = 0.05
Global $volume_change_system = 2000

Global $portsList = _CommListPorts(0)

Global $com_ports_tray_ids[$portsList[0]]
Global $com_ports_numbers[$portsList[0]]

Global $gui_exit_id
Global $gui_separator_id

setup()
main_loop()

Func change_port($port_number)
	; Start up communication with the Arduino
	_CommSetPort($port_number, $sportSetError, $CmBoBaud, $CmboDataBits, $CmBoParity, $CmBoStop, $setflow)
	$DefaultCmPort = $port_number
EndFunc

Func setup()
	; Setup volume changer
	load_volume_changer_config_files()

	; Initialize all apps switches to false.
	; At startup, arduino device must send initial state of switches via commands A1+, A1-, ...
	For $i = 0 To 3
		$activated_apps_switches[$i] = False
	Next

	; System tray icon options

	Opt("TrayMenuMode", 1) ; Hide default tray items

	change_port($DefaultCmPort)
	updateComPortList()
	TraySetState($TRAY_ICONSTATE_SHOW)
	SoundPlay("beep-attention.wav", 0)

EndFunc

Func main_loop()

	Local $hTimerIcon ; Démarre un chrono et stocke Le handle dans une variable.
    Local $fDiffIcon = 0, $iconActive = false

	Local $hTimerDevice ; Démarre un chrono et stocke Le handle dans une variable.
    Local $fDiffDevice = 0, $buffStart = false, $buffCmd = ''

	While True
		$in_str = ''
		$cmd = ''
		; si on reçoit quelque chose
		If (Int(_CommGetInputCount()) > 0) Then
			$in_str = _CommGetString()
		EndIf

		; si on a reçu quelque chose
		If $in_str <> '' Then
			$in_str = StringSplit( $in_str, @CRLF, $STR_ENTIRESPLIT)
			$cmd = StringStripWS ($in_str[UBound($in_str) - 2], $STR_STRIPALL)
			ConsoleWrite($cmd & @CRLF)
			;For $i = 1 To UBound($in_str) - 1
				;ConsoleWrite($in_str[$i])
				;apply_command(StringStripWS ($in_str[$i], $STR_STRIPALL))
			;Next
			TraySetIcon("icon_audio_control_active.ico");
			If $cmd = 'DVR' Or $cmd = 'DHS' Or $cmd = 'DSK' Then
				$buffCmd = $cmd
				$hTimerDevice = TimerInit()
				$buffStart = true
			Else
				apply_command(StringStripWS ($cmd, $STR_STRIPALL))
				SoundPlay("tap-simple.wav", 0)
			EndIf
			$hTimerIcon = TimerInit()
			$iconActive = true
		EndIf
		$fDiffIcon = TimerDiff($hTimerIcon)

        If $iconActive And $fDiffIcon > 200 Then
			TraySetIcon("icon_audio_control.ico")
			$iconActive = false
		EndIf

		$fDiffDevice = TimerDiff($hTimerDevice)

        If $buffStart And $fDiffDevice > 200 Then
			apply_command(StringStripWS ($buffCmd, $STR_STRIPALL))
			;SoundPlay("beep-attention.wav", 0)
			$buffStart = false
			$buffCmd = ''
		EndIf

		; Gestion Interface
		; $action = id du genre d'event de la souris (mouse_down | mouse_click, ...)
		$action = TrayGetMsg()
		If $action = -7 Then
			updateComPortList() ; update on click
		EndIf
		Switch $action
			Case $gui_exit_id
				stop()
			Case Else
				; cherche quel item a été cliquer
				For $i = 0 To UBound($com_ports_tray_ids) - 1
					; intem trouvé, changement de port
					If $action = $com_ports_tray_ids[$i] Then
						change_port($com_ports_numbers[$i])
						updateComPortList() ;
					EndIf
				Next
		EndSwitch
	WEnd
EndFunc

Func apply_command($cmd)
	Switch $cmd
		case "MPR" ; Media Previous
			media_previous()

		case "MPL" ; Media Playpause
			media_playpause()

		case "MNX" ; Media Next
			media_next()

		case "DVR" ; Device VR
			set_vr_as_default_device()

		case "DHS" ; Device Headset
			set_headset_as_default_device()

		case "DSK" ; Device Speakers
			set_speakers_as_default_device()

		case "VS+" ; Volume System Increment
			change_system_volume($volume_change_system)

		case "VS-" ; Volume System Decrement
			change_system_volume(-1 * $volume_change_system)

		case "VF+" ; Volume Focused App Increment
			change_focused_app_volume($volume_change_focused_app)

		case "VF-" ; Volume Focused App Decrement
			change_focused_app_volume(-1 * $volume_change_focused_app)

		case "VA+" ; Volume Selected Apps Increment
			For $i = 0 To 3
				If $activated_apps_switches[$i] Then
					change_selected_apps_volume($i + 1, $volume_change_selected_apps)
				EndIf
			Next

		case "VA-" ; Volume Selected Apps Decrement
			For $i = 0 To 3
				If $activated_apps_switches[$i] Then
					change_selected_apps_volume($i + 1, -1 * $volume_change_selected_apps)
				EndIf
			Next

		case "A1+" ; Set Apps 1
			$activated_apps_switches[0] = True

		case "A1-" ; Unset Apps 1
			$activated_apps_switches[0] = False

		case "A2+" ; Set Apps 2
			$activated_apps_switches[1] = True

		case "A2-" ; Unset Apps 2
			$activated_apps_switches[1] = False

		case "A3+" ; Set Apps 3
			$activated_apps_switches[2] = True

		case "A3-" ; Unset Apps 3
			$activated_apps_switches[2] = False

		case "A4+" ; Set Apps 4
			$activated_apps_switches[3] = True

		case "A4-" ; Unset Apps 4
			$activated_apps_switches[3] = False

		case "VSM" ; Mute system
			mute_system_volume()

		case "VFM" ; Mute focused
			mute_focused_volume()

		case "VAM" ; Mute Apps
			For $i = 0 To 3
				If $activated_apps_switches[$i] Then
					mute_app_volume($i + 1)
				EndIf
			Next
	EndSwitch
EndFunc

Func updateComPortList()
	$portsList = _CommListPorts(0)

	; si on est connecté,
	If (_CommPortConnection() <> '') Then
		$DefaultCmPort = StringTrimLeft(_CommPortConnection(), 3)
	EndIf

	; reset le tray icon menu
	TrayItemDelete($gui_separator_id)
	TrayItemDelete($gui_exit_id)
	For $i = 0 To UBound($com_ports_tray_ids) - 1
		TrayItemDelete($com_ports_tray_ids[$i])
	Next

	; regénère un nouveau menu
	ReDim $com_ports_tray_ids[$portsList[0]]
	ReDim $com_ports_numbers[$portsList[0]]
	For $i = 1 To UBound($portsList) - 1
		$com_ports_numbers[$i - 1] = Int(StringRight("" & $portsList[$i], 1))
		$com_ports_tray_ids[$i - 1] = TrayCreateItem ("" & $portsList[$i], -1, -1, 1)
		if $DefaultCmPort = $com_ports_numbers[$i - 1] Then
			TrayItemSetState(-1, $TRAY_CHECKED)
			change_port($DefaultCmPort)
		EndIf
	Next


	$gui_separator_id = TrayCreateItem("")

	$gui_exit_id = TrayCreateItem("Exit", -1, -1, 0)
EndFunc

Func stop()
    _CommClosePort()
    Exit
EndFunc