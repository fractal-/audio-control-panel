
#include <ScreenCapture.au3>
#include <FileConstants.au3>
#include <File.au3>
#include <StringConstants.au3>

Global $switch[4][32]

Func load_volume_changer_config_files()
	; Read program names from config files
	$file_ext = ".switch"
	For $i = 0 To 3
		$file_handler = FileOpen(($i + 1) & $file_ext, $FO_READ)
		Local $cpt = 0
		While True
			$line = FileReadLine($file_handler)
			If @error = -1  Or $cpt = 31 Then
				While $cpt < 32
					$switch[$i][$cpt] = -1
					$cpt += 1
				WEnd

				ExitLoop
			EndIf

			$line = StringReplace($line, @CRLF, "")

			$switch[$i][$cpt] = $line

			$cpt += 1
		WEnd
	Next
EndFunc


; To be called to change the volume of switch $peripheral_id (from 1 to 4).
; $volume_change must be in grange [-1.0 ; +1.0]
Func change_selected_apps_volume($peripheral_id, $volume_change)
	If $peripheral_id < 1 Or $peripheral_id > 4 Then
		Return
	EndIf

	$cpt = 0
	$program_name = $switch[$peripheral_id - 1][$cpt]
	While $program_name <> -1
		 Run("nircmd.exe changeappvolume " & $program_name & " " & $volume_change)
		$cpt += 1
		$program_name = $switch[$peripheral_id - 1][$cpt]
	WEnd

EndFunc

Func change_system_volume($volume_change)
	Run("nircmd.exe changesysvolume " & $volume_change)
EndFunc

Func change_focused_app_volume($volume_change)
	Run("nircmd.exe changeappvolume focused " & $volume_change)
EndFunc

Func set_headset_as_default_device()
	Run('nircmd.exe setdefaultsounddevice "Headset 5.1"')
EndFunc

Func set_vr_as_default_device()
	Run('nircmd.exe setdefaultsounddevice "Oculus Rift"')
EndFunc

Func set_speakers_as_default_device()
	Run('nircmd.exe setdefaultsounddevice "Speakers 7.1"')
EndFunc

Func mute_system_volume()
	Run('nircmd.exe mutesysvolume 2')
EndFunc

Func mute_focused_volume()
	Run('nircmd.exe muteappvolume focused 2')
EndFunc

Func mute_app_volume($peripheral_id)
	If $peripheral_id < 1 Or $peripheral_id > 4 Then
		Return
	EndIf

	$cpt = 0
	$program_name = $switch[$peripheral_id - 1][$cpt]
	While $program_name <> -1
		 Run("nircmd.exe muteappvolume " & $program_name & " 2")
		$cpt += 1
		$program_name = $switch[$peripheral_id - 1][$cpt]
	WEnd

EndFunc

Func media_playpause()
	Send("{MEDIA_PLAY_PAUSE}")
EndFunc

Func media_previous()
	Send("{MEDIA_PREV}")
EndFunc

Func media_next()
	Send("{MEDIA_NEXT}")
EndFunc

