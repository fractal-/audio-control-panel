#include <SimpleRotary.h>

/* Audio Control Panel
   Made by
   Jeremie GABOLDE
*/

//PINS

// LEDS
const int LED_headset_red = 3; //headset_red(off)
const int LED_headset_green = 4; //headset_green(on)

const int LED_blink = 13;

// Button Power

const int BUTTON_power = 2;
bool BUTTON_power_lastState;

// BUTTONS Switch Device
const int BUTTON_oculus = 6; //key
bool BUTTON_oculus_lastState;
const int BUTTON_speaker = 5;
bool BUTTON_speaker_lastState;

bool BUTTON_headset_lastState;

char* nextDevice = "";
unsigned long nextTimerCheck = 0;
const unsigned int nextTimerCheckPeriod= 100;

// BUTTONS Volume
const int BUTTON_general = 0;
bool BUTTON_general_lastState = HIGH;
const int BUTTON_focused = 16;
bool BUTTON_focused_lastState = HIGH;;
const int BUTTON_selected = 19;
bool BUTTON_selected_lastState = HIGH;;

//SHIFTIN
const int latchPin = 7;
const int clockPin = 8;
const int dataPin = 9;

byte shiftinValue = 72;  //01001000 avoid bug

//BUTTONS ON SHIFTIN
const byte BUTTON_selected1 = 2;
const byte BUTTON_selected2 = 3;
const byte BUTTON_selected3 = 1;
const byte BUTTON_previous = 6;
const byte BUTTON_playpause = 5;
const byte BUTTON_next = 7;

bool BUTTON_selected1_lastState;
bool BUTTON_selected2_lastState;
bool BUTTON_selected3_lastState;
bool BUTTON_previous_lastState;
bool BUTTON_playpause_lastState;
bool BUTTON_next_lastState;

//ROTARY COUNTERS
SimpleRotary rotaryGeneral(A1, A0, 0);
SimpleRotary rotaryFocused(A4, A3, A2);
SimpleRotary rotarySelected(A7, A6, A5);
byte StateGeneral = 0;
byte StateFocused = 0;
byte StateSelected = 0;

// the setup routine runs once when you press reset:
void setup() {
  //Serial
  Serial.begin(9600);

  // initialize LEDS
  pinMode(LED_headset_red, OUTPUT);
  digitalWrite(LED_headset_red, LOW); //off just in case
  pinMode(LED_headset_green, OUTPUT);
  digitalWrite(LED_headset_green, LOW); //off just in case

  pinMode(LED_blink, OUTPUT);
  digitalWrite(LED_blink, HIGH); //Lit during init

  // initialize BUTTONS
  pinMode(BUTTON_power, INPUT);
  
  pinMode(BUTTON_oculus, INPUT); //_PULLUP ?
  pinMode(BUTTON_speaker, INPUT); //_PULLUP ?
  
  pinMode(BUTTON_general, INPUT_PULLUP);
  pinMode(BUTTON_focused, INPUT_PULLUP);
  pinMode(BUTTON_selected, INPUT_PULLUP);

  //initialize shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, INPUT);

  //read shiftin buttons
  //Pulse the latch pin:
  //set it to 1 to collect parallel data
  digitalWrite(latchPin, 1);
  //set it to 1 to collect parallel data, wait
  delayMicroseconds(20);
  //set it to 0 to transmit data serially
  digitalWrite(latchPin, 0);

  //while the shift register is in serial mode
  //collect each shift register into a byte
  //the register attached to the chip comes in first
  shiftinValue = shiftIn(dataPin, clockPin);
  digitalWrite(clockPin, LOW);

  BUTTON_selected1_lastState = checkShiftInValue(shiftinValue, BUTTON_selected1);
  BUTTON_selected2_lastState = checkShiftInValue(shiftinValue, BUTTON_selected2);
  BUTTON_selected3_lastState = checkShiftInValue(shiftinValue, BUTTON_selected3);

  BUTTON_previous_lastState = checkShiftInValue(shiftinValue, BUTTON_previous);
  BUTTON_playpause_lastState = checkShiftInValue(shiftinValue, BUTTON_playpause);
  BUTTON_next_lastState = checkShiftInValue(shiftinValue, BUTTON_next);
  
  BUTTON_power_lastState = !digitalRead(BUTTON_power);

  BUTTON_oculus_lastState = !(digitalRead(BUTTON_oculus) == HIGH);
  BUTTON_speaker_lastState = !(digitalRead(BUTTON_speaker) == HIGH);
  BUTTON_headset_lastState = !(BUTTON_oculus_lastState && BUTTON_speaker_lastState);


  //Init rotary encoders
  rotaryGeneral.setDebounceDelay(2);
  rotaryGeneral.setErrorDelay(100);
  
  rotaryFocused.setDebounceDelay(2);
  rotaryFocused.setErrorDelay(100);
  
  rotarySelected.setDebounceDelay(2);
  rotarySelected.setErrorDelay(100);
  
  sendMessage("ACP");
}

    
// the loop routine runs over and over again forever:
void loop() {
  //delay ? ok
  delay(5);

  // power button
  if (digitalRead(BUTTON_power) == LOW && BUTTON_power_lastState == HIGH) {    
    sendMessage("PW-");
    digitalWrite(LED_headset_red, LOW); //off
    digitalWrite(LED_headset_green, LOW); //off
  }
  else if (digitalRead(BUTTON_power) == HIGH && BUTTON_power_lastState == LOW)
  {
    sendMessage("PW+");
  }
  BUTTON_power_lastState = digitalRead(BUTTON_power);
  if (digitalRead(BUTTON_power) == LOW) return; //Power is off, nothing to do more;

  // Output device selection
  if (digitalRead(BUTTON_oculus) == HIGH) // = pressed
  {
    if (!BUTTON_oculus_lastState)
    {
      nextDevice = "DVR";
      nextTimerCheck = millis() + nextTimerCheckPeriod;
    }
    digitalWrite(LED_headset_red, LOW); //on
    digitalWrite(LED_headset_green, HIGH); //off
  }
  else if (digitalRead(BUTTON_speaker) == HIGH)
  {
    if (!BUTTON_speaker_lastState)
    {
      nextDevice = "DSK";
      nextTimerCheck = millis() + nextTimerCheckPeriod;
    }
    digitalWrite(LED_headset_red, LOW); //on
    digitalWrite(LED_headset_green, HIGH); //off
  }
  else if (digitalRead(BUTTON_oculus) == LOW && digitalRead(BUTTON_speaker) == LOW)
  {
    if (!BUTTON_headset_lastState)
    {
      nextDevice = "DHS";
      nextTimerCheck = millis() + nextTimerCheckPeriod;
    }
    digitalWrite(LED_headset_red, HIGH); //off
    digitalWrite(LED_headset_green, LOW); //on
  }
  BUTTON_oculus_lastState = (digitalRead(BUTTON_oculus) == HIGH);
  BUTTON_speaker_lastState = (digitalRead(BUTTON_speaker) == HIGH);
  BUTTON_headset_lastState = !BUTTON_oculus_lastState && !BUTTON_speaker_lastState;

  if (millis() >  nextTimerCheck && nextDevice != "")
  {
    sendMessage(nextDevice);
    nextDevice = "";
    //nextTimerCheck += nextTimerCheckPeriod;
  }


  if (digitalRead(BUTTON_general) == LOW && BUTTON_general_lastState == HIGH) // = pressed
  {
    //delay(50);
    sendMessage("VSM");
  }
  else if (digitalRead(BUTTON_focused) == LOW && BUTTON_focused_lastState == HIGH) // = pressed
  {
    //delay(50);
    sendMessage("VFM");
  }
  else if (digitalRead(BUTTON_selected) == LOW && BUTTON_selected_lastState == HIGH) // = pressed
  {
    //delay(50);
    sendMessage("VAM");
  }

  BUTTON_general_lastState = digitalRead(BUTTON_general);
  BUTTON_focused_lastState = digitalRead(BUTTON_focused);
  BUTTON_selected_lastState = digitalRead(BUTTON_selected);



  //read shiftin buttons
  //Pulse the latch pin:
  //set it to 1 to collect parallel data
  digitalWrite(latchPin, 1);
  //wait
  delayMicroseconds(20);
  //set it to 0 to transmit data serially
  digitalWrite(latchPin, 0);

  //while the shift register is in serial mode
  //collect each shift register into a byte
  //the register attached to the chip comes in first
  shiftinValue = shiftIn(dataPin, clockPin);
  digitalWrite(clockPin, LOW);
  //Serial.println(shiftinValue, BIN);

  //Button_select_1
  if (checkShiftInValue(shiftinValue, BUTTON_selected1) && !BUTTON_selected1_lastState) sendMessage("A1+");
  if (!checkShiftInValue(shiftinValue, BUTTON_selected1) && BUTTON_selected1_lastState) sendMessage("A1-");

  //Button_select_2
  if (checkShiftInValue(shiftinValue, BUTTON_selected2) && !BUTTON_selected2_lastState) sendMessage("A2+");
  if (!checkShiftInValue(shiftinValue, BUTTON_selected2) && BUTTON_selected2_lastState) sendMessage("A2-");

  //Button_select_3
  if (checkShiftInValue(shiftinValue, BUTTON_selected3) && !BUTTON_selected3_lastState) sendMessage("A3+");
  if (!checkShiftInValue(shiftinValue, BUTTON_selected3) && BUTTON_selected3_lastState) sendMessage("A3-");

  //Button_previous
  if (checkShiftInValue(shiftinValue, BUTTON_previous) && !BUTTON_previous_lastState) sendMessage("MPR");

  //Button_play_pause
  if (checkShiftInValue(shiftinValue, BUTTON_playpause) && !BUTTON_playpause_lastState) sendMessage("MPL");

  //Button_next
  if (checkShiftInValue(shiftinValue, BUTTON_next) && !BUTTON_next_lastState) sendMessage("MNX");

  BUTTON_selected1_lastState = checkShiftInValue(shiftinValue, BUTTON_selected1);
  BUTTON_selected2_lastState = checkShiftInValue(shiftinValue, BUTTON_selected2);
  BUTTON_selected3_lastState = checkShiftInValue(shiftinValue, BUTTON_selected3);

  BUTTON_previous_lastState = checkShiftInValue(shiftinValue, BUTTON_previous);
  BUTTON_playpause_lastState = checkShiftInValue(shiftinValue, BUTTON_playpause);
  BUTTON_next_lastState = checkShiftInValue(shiftinValue, BUTTON_next);


  //rotary buttons
  StateGeneral = rotaryGeneral.rotate();
  if ( StateGeneral == 1 ) //Turned Clockwise
  {
    sendMessage("VS+");
  }

  if ( StateGeneral == 2 ) //Turned Counter-Clockwise
  {
    sendMessage("VS-");
  }

  StateFocused = rotaryFocused.rotate();
  if ( StateFocused == 1 ) //Turned Clockwise
  {
    sendMessage("VF+");
  }

  if ( StateFocused == 2 ) //Turned Counter-Clockwise
  {
    sendMessage("VF-");
  }
  
  StateSelected = rotarySelected.rotate();
  if ( StateSelected == 1 ) //Turned Clockwise
  {
    sendMessage("VA+");
  }

  if ( StateSelected == 2 ) //Turned Counter-Clockwise
  {
    sendMessage("VA-");
  }
  
  //End Loop
  
  digitalWrite(LED_blink, LOW); // LED Blink default state
}

//Functions

bool checkShiftInValue(byte _shiftinValue, byte _shiftInCheck) //compares shiftin data value with a button pin number
{
  return (_shiftinValue & (1 << (_shiftInCheck - 1)));
}

void sendMessage(char* serialMessage) // sends a massage through serial
{
  digitalWrite(LED_blink, HIGH); //Blinks when sending message
  Serial.println(serialMessage);
}




